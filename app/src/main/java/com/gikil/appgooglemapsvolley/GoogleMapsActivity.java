package com.gikil.appgooglemapsvolley;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.gikil.appgooglemapsvolley.countryinfo.CityCoordinates;
import com.gikil.appgooglemapsvolley.countryinfo.Country;
import com.gikil.appgooglemapsvolley.countryinfo.CountryCoordinates;
import com.gikil.appgooglemapsvolley.retrofit.ServiceGenerator;
import com.gikil.appgooglemapsvolley.weatherinfo.WeatherModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Callback;

public class GoogleMapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private String url;
    private ListView listOfCountries;

    //Weather dialog
    private AlertDialog.Builder builder;
    private View dialogView;
    private AlertDialog weatherDialog;
    private TextView weatherTitle;
    private TextView weatherDescription;
    private TextView weatherTemperature;
    private TextView weatherPressure;
    private TextView weatherHumidity;
    private ImageView weatherIcon;

    private ArrayList<Country> countries = new ArrayList<>();
    private ArrayList<String> capitalCitiesList = new ArrayList<>();
    private ArrayList<String> countryNamesList = new ArrayList<>();
    private ArrayAdapter<String> listAdapter;

    private MediaPlayer mp;
    private LatLng city;
    private Marker myMarker;
    private Polygon polygon;
    private GoogleMap mMap;
    private LocationManager locationManager;
    private LocationListener locationListener;

    private String cityName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_city);
        mapFragment.getMapAsync(this);

        initializeMaps();

        listOfCountries.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mp.start();
                city = new LatLng(countries.get(position).getCityGeoPosition().getLatitude(), countries.get(position).getCityGeoPosition().getLongitude());
                cityName = countries.get(position).getCapitalCityName();
                mMap.clear();
                myMarker = mMap.addMarker(new MarkerOptions().position(city).title("Capital city: " + countries.get(position).getCapitalCityName()));
                myMarker.showInfoWindow();
                mMap.setOnMarkerClickListener(GoogleMapsActivity.this);
                polygon = mMap.addPolygon(new PolygonOptions()
                        .add(
                                new LatLng(countries.get(position).getCountryGeoRectangle().getNorth(), countries.get(position).getCountryGeoRectangle().getWest()),
                                new LatLng(countries.get(position).getCountryGeoRectangle().getNorth(), countries.get(position).getCountryGeoRectangle().getEast()),
                                new LatLng(countries.get(position).getCountryGeoRectangle().getSouth(), countries.get(position).getCountryGeoRectangle().getEast()),
                                new LatLng(countries.get(position).getCountryGeoRectangle().getSouth(), countries.get(position).getCountryGeoRectangle().getWest())
                        )
                        .strokeColor(Color.RED));
                ArrayList<LatLng> points = new ArrayList<LatLng>(polygon.getPoints());
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for (LatLng coordinate : points) {
                    builder.include(coordinate);
                }
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 20),3000,null);
            }
        });
    }

    public void exitApp(View view) {
        mp.start();
        GoogleMapsActivity.this.finish();
    }

    private void initializeMaps() {
        url = "http://www.geognos.com/api/en/countries/info/all.json";
        listOfCountries = (ListView) findViewById(R.id.listview_country_names);

        listAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, countryNamesList);
        listOfCountries.setAdapter(listAdapter);

        mp = MediaPlayer.create(this, R.raw.button_click);

        getCountriesInfo(url);

    }

    private void getCountriesInfo(String url) {

        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

            JSONObject results = null;
            JSONObject country = null;
            JSONArray capitalGeoPosition = null;
            JSONObject countryCoord = null;
            String countryId = "";

            @Override
            public void onResponse(JSONObject response) {

                try {
                    results = response.getJSONObject("Results");
                    Iterator<String> keys = results.keys();
                    while (keys.hasNext()) {
                        countryId = keys.next();
                        country = results.getJSONObject(countryId);
                        if (!country.isNull("Capital")) {
                            capitalGeoPosition = country.getJSONObject("Capital").getJSONArray("GeoPt");
                            countryCoord = country.getJSONObject("GeoRectangle");
                            countries.add(new Country(
                                            countryId,
                                            country.getString("Name"),
                                            country.getJSONObject("Capital").getString("Name"),
                                            country.getString("TelPref"),
                                            country.getString("CountryInfo"),
                                            "http://www.geognos.com/api/en/countries/flag/" + countryId + ".png",
                                            new CityCoordinates(country.getJSONObject("Capital").getString("Name"), capitalGeoPosition.getDouble(0), capitalGeoPosition.getDouble(1)),
                                            new CountryCoordinates(country.getString("Name"), countryCoord.getDouble("East"), countryCoord.getDouble("West"), countryCoord.getDouble("North"), countryCoord.getDouble("South"))
                                    )
                            );
                            capitalCitiesList.add(country.getJSONObject("Capital").getString("Name"));
                            countryNamesList.add(country.getString("Name"));
                        }
                    }
                    Collections.sort(countryNamesList);
                    Collections.sort(countries);
                    listAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                // display error
                Toast.makeText(GoogleMapsActivity.this, "Error downloading country info!", Toast.LENGTH_SHORT).show();
            }
        });

        // Add the request to the RequestQueue.
        queue.add(jsObjRequest);
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.equals(myMarker)) {
            getWeatherInfo(cityName);
        }
        return false;
    }

    private void getWeatherInfo(String city) {

        Call<WeatherModel> call = ServiceGenerator.createService(WeatherProviderClient.class)
                .getCityWeather(city);

        call.enqueue(new Callback<WeatherModel>() {
            @Override
            public void onResponse(Call<WeatherModel> call, retrofit2.Response<WeatherModel> response) {

                if (response == null || response.body() == null) {
                    Toast.makeText(getApplicationContext(),"Weather data not available!",Toast.LENGTH_SHORT).show();
                    return;
                }
                WeatherModel weatherInfo = response.body();
                Double temp = 0.0;
                Double humidity = 0.0;
                Double pressure = 0.0;
                String description = "";
                String weatherIconLink = "";  // http://openweathermap.org/img/w/10d.png

                if(weatherInfo.weather.get(0) != null) {
                    description = weatherInfo.weather.get(0).main + " (" + weatherInfo.weather.get(0).description + ")";
                    weatherIconLink = "http://openweathermap.org/img/w/" + weatherInfo.weather.get(0).icon + ".png";
                }
                if(weatherInfo.main !=null) {
                    temp = weatherInfo.main.temp;
                    pressure = weatherInfo.main.pressure;
                    humidity = weatherInfo.main.humidity;
                }

                dialogView = getLayoutInflater().inflate(R.layout.dialog_weather_info, null);
                builder = new AlertDialog.Builder(GoogleMapsActivity.this);
                setWeatherDialog(description, temp, pressure, humidity, weatherIconLink);
                builder.setPositiveButton("OKAY", null).setTitle("Weather info");
                if (dialogView.getParent() == null) {
                    builder.setView(dialogView);
                }
                // create alert dialog
                weatherDialog = builder.create();
                // show it
                weatherDialog.show();
            }

            @Override
            public void onFailure(Call<WeatherModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.getMessage() + " ; " + t.getCause(),Toast.LENGTH_LONG).show();
            }
        });

    }

    private void setWeatherDialog(String description, Double temp, Double pressure, Double humidity, String weatherIconLink) {
        //Weather dialog
        weatherTitle = (TextView) dialogView.findViewById(R.id.title);
        weatherDescription = (TextView) dialogView.findViewById(R.id.weather_desc);
        weatherTemperature = (TextView) dialogView.findViewById(R.id.weather_temp);
        weatherPressure = (TextView) dialogView.findViewById(R.id.weather_press);
        weatherHumidity = (TextView) dialogView.findViewById(R.id.weather_hum);
        weatherIcon = (ImageView) dialogView.findViewById(R.id.weather_icon);

        weatherTitle.setText("City: " + cityName);
        weatherDescription.setText("Description: " + description);
        weatherTemperature.setText("Temperature: " + String.format("%.2f °C", temp - 273.15));
        weatherPressure.setText("Pressure: " + pressure + " mbar");
        weatherHumidity.setText("Humidity: " + humidity + " %");

        try {
            Picasso.with(GoogleMapsActivity.this)
                    .load(weatherIconLink)
                    .placeholder(R.drawable.img_placeholder)
                    .error(R.drawable.img_error)
                    .into(weatherIcon);
        } catch (Exception e) {
            Toast.makeText(GoogleMapsActivity.this, "Bad weather icon link!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                /*LatLng newLocation = new LatLng(location.getLatitude(),location.getLongitude());
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(newLocation).title("New location!"));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(newLocation, 15));*/
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        if (Build.VERSION.SDK_INT < 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }
        else {
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
            }
            else {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }
    }

}
