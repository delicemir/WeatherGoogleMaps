package com.gikil.appgooglemapsvolley;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SplashScreenActivity extends AppCompatActivity {

    private TextView splashText;
    private ImageView imgSplash;
    private LinearLayout mainHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        splashText = (TextView) findViewById(R.id.text_splash);
        imgSplash = (ImageView) findViewById(R.id.img_splash);
        mainHolder = (LinearLayout) findViewById(R.id.main_holder_splash);

        Animation splashAnimation = AnimationUtils.loadAnimation(this,R.anim.animate);
        mainHolder.startAnimation(splashAnimation);

        int secondsDelayed = 4;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(SplashScreenActivity.this, StartupActivity.class));
                finish();
            }
        }, secondsDelayed * 1000);
    }
}
