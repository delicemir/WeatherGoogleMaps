package com.gikil.appgooglemapsvolley;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class StartupActivity extends AppCompatActivity {

    private Button btnMap;
    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        initializeStartup();

        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                Intent maps = new Intent(StartupActivity.this,GoogleMapsActivity.class);
                startActivity(maps);
                finish();
            }
        });
    }

    private void initializeStartup() {

        btnMap = (Button) findViewById(R.id.btn_map);

        mp= MediaPlayer.create(this,R.raw.button_click);

    }
}
