package com.gikil.appgooglemapsvolley;

import com.gikil.appgooglemapsvolley.weatherinfo.WeatherModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by emir on 11/27/17.
 */

public interface WeatherProviderClient {

    @GET("?APPID=95f07868c9e0389cbf15612c9a59434d")
    Call<WeatherModel> getCityWeather(@Query("q") String cityName);

}
