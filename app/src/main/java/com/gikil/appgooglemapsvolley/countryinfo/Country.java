package com.gikil.appgooglemapsvolley.countryinfo;

import android.support.annotation.NonNull;

public class Country implements Comparable{

    private String countryId;   // BA
    private String countryName;  // Bosnia and Herzegovina
    private String capitalCityName; // Sarajevo
    private String telephoneCountryCode;  // 387
    private String countryWebInfo;  // web link
    private String imgFlagWebLink;
    private CityCoordinates cityGeoPosition;
    private CountryCoordinates countryGeoRectangle;

    public Country() {
    }

    public Country(String countryId, String countryName, String capitalCityName, String telephoneCountryCode, String countryWebInfo,
                   String imgFlagWebLink,CityCoordinates cityGeoPosition,CountryCoordinates countryGeoRectangle) {
        setCountryId(countryId);
        setCountryName(countryName);
        setCapitalCityName(capitalCityName);
        setTelephoneCountryCode(telephoneCountryCode);
        setCountryWebInfo(countryWebInfo);
        setImgFlagWebLink(imgFlagWebLink);
        setCityGeoPosition(cityGeoPosition);
        setCountryGeoRectangle(countryGeoRectangle);
    }

    public String getCountryId() {
        return countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getCapitalCityName() {
        return capitalCityName;
    }

    public String getTelephoneCountryCode() {
        return telephoneCountryCode;
    }

    public String getCountryWebInfo() {
        return countryWebInfo;
    }

    public String getImgFlagWebLink() {
        return imgFlagWebLink;
    }

    public CityCoordinates getCityGeoPosition() {
        return cityGeoPosition;
    }

    public CountryCoordinates getCountryGeoRectangle() {
        return countryGeoRectangle;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public void setCapitalCityName(String capitalCityName) {
        this.capitalCityName = capitalCityName;
    }

    public void setTelephoneCountryCode(String telephoneCountryCode) {
        this.telephoneCountryCode = telephoneCountryCode;
    }

    public void setCountryWebInfo(String countryWebInfo) {
        this.countryWebInfo = countryWebInfo;
    }

    public void setImgFlagWebLink(String imgFlagWebLink) {
        this.imgFlagWebLink = imgFlagWebLink;
    }

    public void setCityGeoPosition(CityCoordinates cityGeoPosition) {
        this.cityGeoPosition = cityGeoPosition;
    }

    public void setCountryGeoRectangle(CountryCoordinates countryGeoRectangle) {
        this.countryGeoRectangle = countryGeoRectangle;
    }

    @Override
    public String toString() {
        return "Country{" +
                "countryId='" + countryId + '\'' +
                ", countryName='" + countryName + '\'' +
                ", capitalCityName='" + capitalCityName + '\'' +
                ", telephoneCountryCode='" + telephoneCountryCode + '\'' +
                ", countryWebInfo='" + countryWebInfo + '\'' +
                ", imgFlagWebLink='" + imgFlagWebLink + '\'' +
                ", cityGeoPosition=" + cityGeoPosition +
                ", countryGeoRectangle=" + countryGeoRectangle +
                '}';
    }

    @Override
    public int compareTo(@NonNull Object o) {
        Country country = (Country) o;
        return getCountryName().compareTo(country.getCountryName());
    }

}



