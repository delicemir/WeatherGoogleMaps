package com.gikil.appgooglemapsvolley.countryinfo;

public class CountryCoordinates {

    private String countryName;
    private Double east;
    private Double west;
    private Double north;
    private Double south;

    public CountryCoordinates(){};

    public CountryCoordinates(String countryName, Double east, Double west, Double north, Double south) {
        setCountryName(countryName);
        setEast(east);
        setWest(west);
        setNorth(north);
        setSouth(south);
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Double getEast() {
        return east;
    }

    public void setEast(Double east) {
        this.east = east;
    }

    public Double getWest() {
        return west;
    }

    public void setWest(Double west) {
        this.west = west;
    }

    public Double getNorth() {
        return north;
    }

    public void setNorth(Double north) {
        this.north = north;
    }

    public Double getSouth() {
        return south;
    }

    public void setSouth(Double south) {
        this.south = south;
    }

    @Override
    public String toString() {
        return "CountryCoordinates{" +
                "east=" + east +
                ", west=" + west +
                ", north=" + north +
                ", south=" + south +
                '}';
    }
}
