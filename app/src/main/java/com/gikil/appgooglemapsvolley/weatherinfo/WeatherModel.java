package com.gikil.appgooglemapsvolley.weatherinfo;

import java.util.List;

/**
 * Created by emir on 11/27/17.
 */

public class WeatherModel {


    public String name ;
    public Coordinates coord;
    public List<Weather> weather;
    public Main main;

    public class Coordinates {
        public Double lon;
        public Double lat;
    }

    public class Weather {
        public String main;
        public String description;
        public String icon;
    }

    public class Main {
        public Double temp;
        public Double pressure;
        public Double humidity;
    }

}
